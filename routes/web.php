<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\SchoolsController;
use App\Http\Controllers\TrackController;
use App\Http\Controllers\StrandController;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\PublicController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route definition...
Route::get('/', [PublicController::class, 'index'])->name('home');

Route::get('/dashboard', [AdminController::class, 'index'])->name('dashboard');
Route::get('/user', [AdminController::class, 'user_info'])->name('user_info');
Route::get('/users', [AdminController::class, 'user_index'])->name('user_index');
Route::get('/user/create', [AdminController::class, 'user_create'])->name('user_create');
Route::post('/user/store', [AdminController::class, 'user_store'])->name('user_store');
Route::get('/user/edit/{id}', [AdminController::class, 'user_edit'])->name('user_edit');
Route::put('/user/edit/{id}', [AdminController::class, 'user_update'])->name('user_update');
Route::delete('/user/delete/{id}', [AdminController::class, 'user_destroy'])->name('user_destroy');

Route::get('/settings', [AdminController::class, 'settings'])->name('settings');
Route::post('/settings/save', [AdminController::class, 'save_settings'])->name('save_settings');

Route::get('/schools', [SchoolsController::class, 'index'])->name('schools');
Route::get('/schools/create', [SchoolsController::class, 'create'])->name('schools_create');
Route::get('/schools/edit/{id}', [SchoolsController::class, 'edit'])->name('schools_edit');
Route::post('/schools/store', [SchoolsController::class, 'store'])->name('schools_store');
Route::put('/schools/edit/{id}', [SchoolsController::class, 'update'])->name('schools_update');
Route::delete('/schools/delete/{id}', [SchoolsController::class, 'destroy'])->name('schools_destroy');

Route::get('/tracks', [TrackController::class, 'index'])->name('tracks');
Route::get('/tracks/create', [TrackController::class, 'create'])->name('tracks_create');
Route::get('/tracks/edit/{id}', [TrackController::class, 'edit'])->name('tracks_edit');
Route::post('/tracks/store', [TrackController::class, 'store'])->name('tracks_store');
Route::put('/tracks/edit/{id}', [TrackController::class, 'update'])->name('tracks_update');
Route::delete('/tracks/delete/{id}', [TrackController::class, 'destroy'])->name('tracks_destroy');

Route::get('/strands', [StrandController::class, 'index'])->name('strands');
Route::get('/strands/create', [StrandController::class, 'create'])->name('strands_create');
Route::get('/strands/edit/{id}', [StrandController::class, 'edit'])->name('strands_edit');
Route::post('/strands/store', [StrandController::class, 'store'])->name('strands_store');
Route::put('/strands/edit/{id}', [StrandController::class, 'update'])->name('strands_update');
Route::delete('/strands/delete/{id}', [StrandController::class, 'destroy'])->name('strands_destroy');

Route::get('/applications', [ApplicationController::class, 'index'])->name('applications');
Route::get('/applications/create', [ApplicationController::class, 'create'])->name('applications_create');
Route::get('/applications/edit/{id}', [ApplicationController::class, 'edit'])->name('applications_edit');
Route::post('/applications/store', [ApplicationController::class, 'store'])->name('applications_store');
Route::put('/applications/edit/{id}', [ApplicationController::class, 'update'])->name('applications_update');
Route::delete('/applications/delete/{id}', [ApplicationController::class, 'destroy'])->name('applications_destroy');

require __DIR__.'/auth.php';