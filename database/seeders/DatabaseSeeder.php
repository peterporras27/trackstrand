<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\School;
use App\Models\Strand;
use App\Models\Track;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $admin = User::create([
            'first_name' => 'Super', 
            'last_name' => 'Admin', 
            'middle_name' => '', 
            'role' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('secret')
        ]);


        $schools = [
            'Janiuay National Comprehensive High School',
            'Calvario Memorial Christian School',
            'St. Julian Academy',
            'Calmay National High School',
            'Major Manuel A. Aaron Memorial National High School',
            'Center Phil Montessori Learning Center, Inc.'
        ];

        foreach ($schools as $school) {
            $newSchool = School::create([
                'name' => $school
            ]);
        }

        $tracks = [
            'Academic Track',
            'TVL Track',
            'Sports Track',
            'Art and Design',
            'Home Economics',
            'Industrial Arts'
        ];

        foreach ($tracks as $track) {
            $newTrack = Track::create([
                'name' => $track
            ]);
        }

        $strands = [
            'Accountancy',
            'Business and Management',
            'Humanities and Social Sciences',
            'Science',
            'Technology',
            'Engineering',
            'Mathematics',
            'General academic',
        ];

        foreach ($strands as $strand) {
            $newStrand = Strand::create([
                'name' => $strand,
                'available_slots' => 50,
                'track_id' => 1,
                'school_id' => 1
            ]);
        }
    }
}
