<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('middle_name')->nullable();
            
            $table->string('phone')->nullable();
            $table->date('birthday')->nullable();
            $table->string('age')->nullable();
            $table->string('gender')->nullable();
            $table->string('civil_status')->nullable();
            $table->string('address')->nullable();
            $table->integer('zip_code')->nullable();
            $table->string('lrn')->nullable();
            $table->string('birth_place')->nullable();
            $table->string('parent_guardian')->nullable();
            $table->string('occupation')->nullable();
            $table->string('disabilities')->nullable();
            $table->string('school_graduated_from')->nullable();
            $table->string('last_school_attended')->nullable();
            $table->string('last_school_address')->nullable();
            $table->string('grade_year_level')->nullable();
            $table->string('photo')->nullable();

            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->enum('role', ['admin','faculty','student'])->default('student');
            $table->integer('school_id')->nullable(); // for faculty
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
