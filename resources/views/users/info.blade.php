<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Application details') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    @if (session('success'))

                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Success!</h4>
                        <p>{{ session('success') }}</p>
                    </div>

                    @endif

                    @if (session('warning'))

                    <div class="alert alert-warning" role="alert">
                        <h4 class="alert-heading">Warning!</h4>
                        <p>{{ session('warning') }}</p>
                    </div>

                    @endif
                    
                    <div class="row">
                        <div class="col-sm-3">
                            @if($user->photo)
                                <img src="{{ asset('storage/'.str_replace('public/','',$user->photo)) }}" class="img-fluid rounded-start img-thumbnail" alt="...">
                            @endif
                            <div class="table-responsive">
                                <table class="table-hover table table-bordered">
                                    <tbody>
                                        <tr><td><b>First Name:</b> {{$user->first_name}}</td></tr>
                                        <tr><td><b>Last Name:</b> {{$user->last_name}}</td></tr>
                                        <tr><td><b>Middle Name:</b> {{$user->middle_name}}</td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="table-responsive">
                                <table class="table-hover table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td><b>Phone:</b> {{$user->phone}}</td>
                                            <td><b>Birthday:</b> {{$user->birthday}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Age:</b> {{$user->age}}</td>
                                            <td><b>Gender:</b> {{$user->gender}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Civil Status:</b> {{$user->civil_status}}</td>
                                            <td><b>Address:</b> {{$user->address}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Zip Code:</b> {{$user->zip_code}}</td>
                                            <td><b>LRN:</b> {{$user->lrn}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Birth Place:</b> {{$user->birth_place}}</td>
                                            <td><b>Parent/Guardian:</b> {{$user->parent_guardian}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Occupation:</b> {{$user->occupation}}</td>
                                            <td><b>Disabilities:</b> {{$user->disabilities}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>School Graduated From:</b> {{$user->school_graduated_from}}</td>
                                            <td><b>Last School Attended:</b> {{$user->last_school_attended}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>School Address:</b> {{$user->last_school_address}}</td>
                                            <td><b>Grade/Year Level:</b> {{$user->grade_year_level}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
