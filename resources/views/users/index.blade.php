<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Users') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    @if (session('success'))

                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Success!</h4>
                        <p>{{ session('success') }}</p>
                    </div>
                    @endif

                    <a href="{{ route('user_create') }}" class="btn btn-info">Create User</a>

                    <br><br>

                    @if($users->count())
                    <table class="table table-bordered table-inverse table-hover table-sm">
                        <thead>
                           <tr>
                               <th>Name</th>
                               <th>Role</th>
                               <th>School</th>
                               <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{ $user->first_name.' '.$user->last_name }}</td>
                                <td>{{ $user->role == 'faculty' ? 'Personnel':ucword($user->role) }}</td>
                                <td>{{ ($user->school) ? $user->school->name:'' }}</td>
                                <td>
                                    <form method="POST" class="float-right" action="{{ route('user_destroy',$user->id) }}">
                                        @csrf
                                        @method('DELETE')
                                        <a href="#" class="float-right btn btn-sm btn-danger" onclick="event.preventDefault();
                                        this.closest('form').submit();">Delete</a>
                                    </form>
                                    <a href="{{ route('user_edit',$user->id) }}" class="float-right mr-2 btn btn-sm btn-info">Edit</a>
                                </td>
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
                    @else
                    <div class="alert alert-warning" role="alert">
                        <p>There are no data's to display at the moment.</p>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
