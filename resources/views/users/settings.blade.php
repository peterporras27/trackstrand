<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Settings') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    @if (session('success'))

                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Success!</h4>
                        <p>{{ session('success') }}</p>
                    </div>

                    @endif

                    @if (session('warning'))

                    <div class="alert alert-warning" role="alert">
                        <h4 class="alert-heading">Warning!</h4>
                        <p>{{ session('warning') }}</p>
                    </div>

                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Error</h4>
                            <ul class="mt-3 list-disc list-inside text-sm text-red-600">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                  
                    <form action="{{ route('save_settings') }}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-sm-4">
                                @if(Auth::user()->photo)
                                <img src="{{ asset('storage/'.str_replace('public/','',Auth::user()->photo)) }}" class="img-fluid rounded-start img-thumbnail" alt="...">
                                @endif
                                <div class="mb-3">
                                    <label for="first_name" class="form-label">Profile Photo:</label>
                                    <input type="file" accept="image/*" name="photo" class="form-control" id="photo">
                                </div>
                                <div class="mb-3">
                                    <label for="first_name" class="form-label">First Name:</label>
                                    <input type="text" value="{{ Auth::user()->first_name }}" name="first_name" class="form-control" id="first_name" placeholder="First Name">
                                </div>
                                <div class="mb-3">
                                    <label for="middle_name" class="form-label">Middle Name:</label>
                                    <input type="text" value="{{ Auth::user()->middle_name }}" name="middle_name" class="form-control" id="middle_name" placeholder="Middle Name">
                                </div>
                                <div class="mb-3">
                                    <label for="last_name" class="form-label">Last Name:</label>
                                    <input type="text" value="{{ Auth::user()->last_name }}" name="last_name" class="form-control" id="last_name" placeholder="Last Name">
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="birthday" class="form-label">Birth Day:</label>
                                            <input type="date" value="{{ Auth::user()->birthday }}" name="birthday" class="form-control" id="birthday" placeholder="02/27/1999">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="birth_place" class="form-label">Birth Place:</label>
                                            <input type="text" value="{{ Auth::user()->birth_place }}" name="birth_place" class="form-control" id="birth_place" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="age" class="form-label">Age:</label>
                                            <input type="number" value="{{ Auth::user()->age }}" name="age" class="form-control" id="age" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="civil_status" class="form-label">Civil Status:</label>
                                            <select name="civil_status" class="form-control" id="civil_status">
                                                <option value="Single"{{ Auth::user()->gender == 'Single' ? ' selected':'' }}>Single</option>
                                                <option value="Married"{{ Auth::user()->gender == 'Married' ? ' selected':'' }}>Married</option>
                                                <option value="Single Parent"{{ Auth::user()->gender == 'Single Parent' ? ' selected':'' }}>Single Parent</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="gender" class="form-label">Gender:</label>
                                            <select name="gender" class="form-control" id="gender">
                                                <option value="male"{{ Auth::user()->gender == 'male' ? ' selected':'' }}>Male</option>
                                                <option value="female"{{ Auth::user()->gender == 'female' ? ' selected':'' }}>Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="phone" class="form-label">Phone:</label>
                                            <input type="number" value="{{ Auth::user()->phone }}" name="phone" class="form-control" id="phone" placeholder="09xxxxxxxxx">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="address" class="form-label">Address:</label>
                                            <input type="text" value="{{ Auth::user()->address }}" name="address" class="form-control" id="address" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="zip_code" class="form-label">Zip Code:</label>
                                            <input type="number" value="{{ Auth::user()->zip_code }}" name="zip_code" class="form-control" id="zip_code" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="parent_guardian" class="form-label">Parent/Guardian:</label>
                                            <input type="text" value="{{ Auth::user()->parent_guardian }}" name="parent_guardian" class="form-control" id="parent_guardian" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="occupation" class="form-label">Occupation:</label>
                                            <input type="text" value="{{ Auth::user()->occupation }}" name="occupation" class="form-control" id="occupation" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                     <div class="col">
                                        <div class="mb-3">
                                            <label for="disabilities" class="form-label">Disabilities:</label>
                                            <input type="text" value="{{ Auth::user()->disabilities }}" name="disabilities" class="form-control" id="disabilities" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="lrn" class="form-label">Learners Reference Number LRN:</label>
                                            <input type="text" value="{{ Auth::user()->lrn }}" name="lrn" class="form-control" id="lrn" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="school_graduated_from" class="form-label">School Graduated From:</label>
                                            <input type="text" value="{{ Auth::user()->school_graduated_from }}" name="school_graduated_from" class="form-control" id="school_graduated_from" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="last_school_attended" class="form-label">Last School Attended:</label>
                                            <input type="text" value="{{ Auth::user()->last_school_attended }}" name="last_school_attended" class="form-control" id="last_school_attended" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="last_school_address" class="form-label">School Address:</label>
                                            <input type="text" value="{{ Auth::user()->last_school_address }}" name="last_school_address" class="form-control" id="last_school_address" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="grade_year_level" class="form-label">Grade/Year Level:</label>
                                            <input type="text" value="{{ Auth::user()->grade_year_level }}" name="grade_year_level" class="form-control" id="grade_year_level" placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <button class="btn btn-primary" style="background-color: #007bff;" type="submit">Save</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
