<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit user') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    @if (session('success'))

                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Success!</h4>
                        <p>{{ session('success') }}</p>
                    </div>

                    @endif

                    @if (session('warning'))

                    <div class="alert alert-warning" role="alert">
                        <h4 class="alert-heading">Warning!</h4>
                        <p>{{ session('warning') }}</p>
                    </div>

                    @endif


                    @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Error</h4>
                            <ul class="mt-3 list-disc list-inside text-sm text-red-600">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                  
                    <form action="{{ route('user_update',$user->id) }}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="first_name" class="form-label">First Name:</label>
                                    <input type="text" name="first_name" value="{{ $user->first_name }}" class="form-control" id="first_name" placeholder="First Name">
                                </div>
                            </div>
                            <div class="col">
                                <div class="mb-3">
                                    <label for="last_name" class="form-label">Last Name:</label>
                                    <input type="text" name="last_name" value="{{ $user->last_name }}" class="form-control" id="last_name" placeholder="Last Name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="middle_name" class="form-label">Middle Name:</label>
                                    <input type="text" name="middle_name" value="{{ $user->middle_name }}" class="form-control" id="middle_name" placeholder="Middle Name">
                                </div>
                            </div>
                            <div class="col">
                                <div class="mb-3">
                                    <label for="phone" class="form-label">Phone:</label>
                                    <input type="text" name="phone" value="{{ $user->phone }}" class="form-control" id="phone" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="email" class="form-label">Email:</label>
                                    <input type="email" name="email" value="{{ $user->email }}" class="form-control" id="email" placeholder="">
                                </div>
                            </div>
                            <div class="col">
                                <div class="mb-3">
                                    <label for="school" class="form-label">School Assigned:</label>
                                    <select name="school_id" class="form-control" id="school">
                                        @if($schools->count())
                                            @foreach($schools as $school)
                                                <option value="{{ $school->id }}"{{ $user->school_id == $school->id ? ' selected':'' }}>{{ $school->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="password" class="form-label">Password:</label>
                                    <input type="password" name="password" class="form-control" id="password">
                                </div>
                            </div>
                            <div class="col">
                                <div class="mb-3">
                                    <label for="password_confirmation" class="form-label">Confirm Password:</label>
                                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation">
                                </div>
                            </div>
                        </div>
                                
                        <button class="btn btn-primary" style="background-color: #007bff;" type="submit">Update</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
