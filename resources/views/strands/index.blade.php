<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Strands') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    @if (session('success'))

                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Success!</h4>
                        <p>{{ session('success') }}</p>
                    </div>
                    @endif

                    <a href="{{ route('strands_create') }}" class="btn btn-info">Register Strand</a>

                    <br><br>

                    @if($strands->count())
                    <table class="table table-bordered table-inverse table-hover table-sm">
                        <thead>
                           <tr>
                               <th>Name</th>
                               <th>Track</th>
                               <th>Slots</th>
                               <th>School</th>
                               <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                            @foreach($strands as $strand)
                            <tr>
                                <td>{{ $strand->name }}</td>
                                <td>{{ ($strand->track) ? $strand->track->name:'' }}</td>
                                <td>
                                    @if($strand->slot_type == 'limitted')
                                        {{ $strand->available_slots }}
                                    @else
                                        Open
                                    @endif
                                </td>
                                <td>{{ ($strand->school) ? $strand->school->name:'' }}</td>
                                <td>
                                    <div class="modal fade" id="modal-{{$strand->id}}" tabindex="-1" aria-labelledby="modal-{{$strand->id}}Label" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <form method="POST" action="{{ route('strands_destroy',$strand->id) }}">
                                                    @csrf
                                                    @method('DELETE')
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="modal-{{$strand->id}}Label">Delete Strand</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Are you sure you wish to premanently delete {{ $strand->name }}?
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="#" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                                                    <a href="#" onclick="event.preventDefault();this.closest('form').submit();"
                                                        class="btn btn-danger">Delete</a>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <a href="#" class="float-right btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-{{$strand->id}}">
                                        Delete
                                    </a>
                                    <a href="{{ route('strands_edit',$strand->id) }}" class="float-right mr-2 btn btn-sm btn-info">Edit</a>
                                </td>
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
                    {{ $strands->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
