<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Strand') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    @if (session('success'))

                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Success!</h4>
                        <p>{{ session('success') }}</p>
                    </div>

                    @endif
                  
                    <form action="{{ route('strands_update',$strand->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="name" class="form-label">Name:</label>
                                    <input type="text" value="{{ $strand->name }}" name="name" class="form-control" id="name" placeholder="Name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="slot_type" class="form-label">Slots Type:</label>
                                    <select name="slot_type" class="form-control" id="slot_type">
                                        <option value="open"{{ $strand->slot_type == 'open' ? ' selected':'' }}>Open</option>
                                        <option value="limitted"{{ $strand->slot_type == 'limitted' ? ' selected':'' }}>Limitted</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col">        
                                <div class="mb-3">
                                    <label for="available_slots" class="form-label">Available Slots if Limitted:</label>
                                    <input type="number" value="{{ $strand->available_slots }}" name="available_slots" class="form-control" id="available_slots" placeholder="10">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="description" class="form-label">Description:</label>
                                    <textarea name="description" class="form-control" id="description" placeholder="Description">{{ $strand->description }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="track_id" class="form-label">Select Track:</label>
                                    <select type="text" name="track_id" class="form-control" id="track_id">
                                        @foreach($tracks as $track)
                                            <option value="{{ $track->id }}"{{ $strand->track_id == $track->id ? ' selected':'' }}>{{ $track->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="mb-3">
                                    <label for="school_id" class="form-label">Select School:</label>
                                    <select type="text" name="school_id" class="form-control" id="school_id">
                                        @foreach($schools as $school)
                                            <option value="{{ $school->id }}"{{ $strand->school_id == $school->id ? ' selected':'' }}>{{ $school->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                        </div>
                        <button class="btn btn-primary" style="background-color: #007bff;" type="submit">Save</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
