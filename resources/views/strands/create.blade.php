<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Register Strand') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    @if (session('success'))

                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Success!</h4>
                        <p>{{ session('success') }}</p>
                    </div>

                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Error!</h4>
                            <ul class="mt-3 list-disc list-inside text-sm text-red-600">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                  
                    <form action="{{ route('strands_store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="name" class="form-label">Name:</label>
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="slot_type" class="form-label">Slots Type:</label>
                                    <select name="slot_type" class="form-control" id="slot_type">
                                        <option value="open">Open</option>
                                        <option value="limitted">Limitted</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col">        
                                <div class="mb-3">
                                    <label for="available_slots" class="form-label">Available Slots if Limitted:</label>
                                    <input type="number" name="available_slots" value="50" class="form-control" id="available_slots" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="description" class="form-label">Description:</label>
                                    <textarea name="description" class="form-control" id="description" placeholder="Description"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                 <div class="mb-3">
                                    <label for="track_id" class="form-label">Select Track:</label>
                                    <select type="text" name="track_id" class="form-control" id="track_id">
                                        @foreach($tracks as $track)
                                            <option value="{{ $track->id }}">{{ $track->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                @if(auth()->user()->role=='faculty')
                                <input type="hidden" name="school_id" value="{{ auth()->user()->school_id }}">
                                @else
                                <div class="mb-3">
                                    <label for="school_id" class="form-label">Select School:</label>
                                    <select type="text" name="school_id" class="form-control" id="school_id">
                                        @foreach($schools as $school)
                                            <option value="{{ $school->id }}">{{ $school->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @endif
                            </div>
                        </div>
                        
                        <button class="btn btn-primary" style="background-color: #007bff;" type="submit">Save</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
