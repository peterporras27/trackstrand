<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Applications') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        <b>Success!</b> {{ session('success') }}
                    </div>
                    @endif

                    @if (session('info'))
                    <div class="alert alert-info" role="alert">
                        <b>Info:</b> {{ session('info') }}
                    </div>
                    @endif

                    @if($applications->count())
                    <div class="table-responsive">
                        <table class="table table-bordered table-inverse table-hover">
                            <thead>
                               <tr>
                                   <th>Name</th>
                                   <th>Strand</th>
                                   <th>School</th>
                                   <th>Status</th>
                               </tr>
                            </thead>
                            <tbody>
                                @foreach($applications as $application)
                                <tr>
                                    <td class="align-middle">
                                        <a href="{{ route('user_info', ['id' => $application->user->id]) }}">
                                        {{ $application->user->first_name.' '.$application->user->middle_name.' '.$application->user->last_name }}
                                        </a>
                                    </td>
                                    <td class="align-middle">{{ ($application->strand) ? $application->strand->name:'' }}</td>
                                    <td class="align-middle">{{ ($application->school) ? $application->school->name:'' }}</td>
                                    <td class="align-middle">
                                        @if( in_array(Auth::user()->role, ['admin','faculty','student']) )
                                            @if($application->status == 'cancelled' && Auth::user()->role == 'student')
                                                {{ucfirst($application->status)}}
                                            @else
                                            <form method="POST" action="{{ route('applications_update',$application->id) }}">
                                                @csrf
                                                @method('PUT')
                                                <div class="input-group">
                                                    <select name="status" class="form-control">
                                                        @if(Auth::user()->role!='student')
                                                            <option value="pending"{{ $application->status == 'pending' ? ' selected':'' }}>Pending</option>
                                                            <option value="approved"{{ $application->status == 'approved' ? ' selected':'' }}>Approve</option>
                                                        @endif
                                                        <option value="cancelled"{{ $application->status == 'cancelled' ? ' selected':'' }}>Cancel</option>
                                                    </select>
                                                    <div class="input-group-append">
                                                        <button type="submit" class="btn btn-outline-secondary">Update</button>
                                                    </div>
                                                </div>
                                            </form>
                                            @endif
                                        @else
                                            {{ $application->status }}
                                        @endif
                                    </td>
                                </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $applications->links() }}
                    @else
                        <div class="alert alert-warning" role="alert">
                            <p>There are no data to display at the moment.</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
