<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Track and Strand</title>
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
		<style>
		a{text-decoration: none;color: #000;}
		body{
			background: url({{asset('img/school.jpg')}}) repeat top center;
			background-size: 50%;
		}
		</style>
	</head>
	<body>
		<main>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					@if (Route::has('login'))
	                    @auth
			                <li class="nav-item active">
								<a class="nav-link" href="{{ url('/dashboard') }}">Dashboard <span class="sr-only">(current)</span></a>
							</li>
	                    @else

							<li class="nav-item my-2 my-lg-0">
								<a class="nav-link" href="{{ route('login') }}">Login</a>
							</li>

	                        @if (Route::has('register'))
	                            <li class="nav-item my-2 my-lg-0">
									<a class="nav-link" href="{{ route('register') }}">Register</a>
								</li>
	                        @endif
	                    @endauth
	            	@endif
				</ul>
			</div>
		</nav>

		<div class="container mt-3">
			<div class="row">
				<div class="col">
					<div class="card text-center mb-3">
						<div class="card-body">
							<h3 class="card-title mb-0 text-uppercase"><b>Senior High School Track and Strand Portal</b></h3>
						</div>
					</div>

					<div class="accordion" id="accordionExample">
                        
                        @foreach($schools as $school)

                        <div class="card">
                            <div class="card-header" id="heading-{{ $school->id }}">
                                <h2 class="mb-0 text-center">
                                    <button class="btn " type="button" data-toggle="collapse" data-target="#collapse-{{ $school->id }}" aria-expanded="false" aria-controls="collapse-{{ $school->id }}">
                                        @if($school->logo)
	                                    <img width="50" style="display: inline-block;" src="{{ asset('storage/'.str_replace('public/','',$school->logo)) }}" class="img-fluid rounded-start img-thumbnail" alt="...">
	                                    @endif 
	                                    <h4 class="mb-0">{{  $school->name }}</h4>
                                    </button>
                                </h2>
                            </div>

                            <div id="collapse-{{ $school->id }}" class="collapse" aria-labelledby="heading-{{ $school->id }}" data-parent="#accordionExample">
                                <div class="card-body">
                                    @foreach($school->tracks() as $track => $strands)

                                        @if(count($strands))
                                        
                                        <div class="card mb-2">
                                            <div class="card-header">
                                                <h6 class="mb-0">{{ $track }}</h6>
                                            </div>
                                            <div class="card-body">
                                                @foreach($strands as $strand)

                                                    <div class="card mb-2">
                                                        <div class="card-header">
                                                            <h6 class="mb-0">
                                                                {{ $strand['name'] }} 
                                                                @if($strand['slot_type']=='limitted')
                                                                (<span class="badge bg-dark rounded-pill text-white">{{ $strand['available_slots'] }}</span> <small>Slots Available</small>)
                                                                @else
                                                                <small>(open)</small>
                                                                @endif
                                                            </h6>
                                                        </div>
                                                    </div>

                                                @endforeach
                                            </div>
                                        </div>

                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
				</div>
			</div>
		</div>
		</main>
		<script src="{{asset('bootstrap/js/jquery-3.6.0.min.js')}}"></script>
        <script src="{{asset('bootstrap/js/bootstrap.min.js')}}" ></script>
        <script src="{{asset('js/script.js')}}" ></script>
	</body>
</html>