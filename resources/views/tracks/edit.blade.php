<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Track') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    @if (session('success'))

                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Success!</h4>
                        <p>{{ session('success') }}</p>
                    </div>

                    @endif
                  
                    <form action="{{ route('tracks_update',$track->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="name" class="form-label">Name:</label>
                                    <input type="text" name="name" value="{{ $track->name }}" class="form-control" id="name" placeholder="Name">
                                </div>
                                <div class="mb-3">
                                    <label for="description" class="form-label">Description:</label>
                                    <textarea name="description" value="{{ $track->description }}" class="form-control" id="description" placeholder="Description"></textarea>
                                </div>
                            </div>
                            
                        </div>
                        <button class="btn btn-primary" style="background-color: #007bff;" type="submit">Save</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
