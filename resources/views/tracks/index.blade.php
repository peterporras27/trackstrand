<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tracks') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    @if (session('success'))

                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Success!</h4>
                        <p>{{ session('success') }}</p>
                    </div>
                    @endif

                    <a href="{{ route('tracks_create') }}" class="btn btn-info">Register Track</a>

                    <br><br>

                    @if($tracks->count())
                    <table class="table table-bordered table-inverse table-hover table-sm">
                        <thead>
                           <tr>
                               <th>Name</th>
                               <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                            @foreach($tracks as $track)
                            <tr>
                                <td>{{ $track->name }}</td>
                                <td>

                                    <div class="modal fade" id="modal-{{$track->id}}" tabindex="-1" aria-labelledby="modal-{{$track->id}}Label" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <form method="POST" action="{{ route('tracks_destroy',$track->id) }}">
                                                    @csrf
                                                    @method('DELETE')
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="modal-{{$track->id}}Label">Delete Track</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Are you sure you wish to premanently delete {{ $track->name }}?
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="#" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                                                    <a href="#" onclick="event.preventDefault();this.closest('form').submit();"
                                                        class="btn btn-danger">Delete</a>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <a href="#" class="float-right btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-{{$track->id}}">
                                        Delete
                                    </a>

                                    <a href="{{ route('tracks_edit',$track->id) }}" class="float-right mr-2 btn btn-sm btn-info">Edit</a>

                                </td>
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
                    {{ $tracks->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
