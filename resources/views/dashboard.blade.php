<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading"><b>Success!</b></h4>
                        <p>{{ session('success') }}</p>
                    </div>
                    @endif

                    @if (session('danger'))
                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading"><b>Oops!</b></h4>
                        <p>{{ session('danger') }}</p>
                    </div>
                    @endif


                    <div class="accordion" id="accordionExample">
                        
                        @foreach($schools as $school)

                        <div class="card">
                            <div class="card-header" id="heading-{{ $school->id }}">
                                <h2 class="mb-0">
                                    @if($school->logo)
                                    <img width="50" style="display: inline-block;" src="{{ asset('storage/'.str_replace('public/','',$school->logo)) }}" class="img-fluid rounded-start img-thumbnail" alt="...">
                                    @endif
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse-{{ $school->id }}" aria-expanded="false" aria-controls="collapse-{{ $school->id }}">
                                        {{ $school->name }}
                                    </button>
                                </h2>
                            </div>

                            <div id="collapse-{{ $school->id }}" class="collapse" aria-labelledby="heading-{{ $school->id }}" data-parent="#accordionExample">
                                <div class="card-body">
                                    @foreach($school->tracks() as $track => $strands)

                                        @if(count($strands))
                                        
                                        <div class="card mb-2">
                                            <div class="card-header">
                                                <h4>{{ $track }}</h4>
                                            </div>
                                            <div class="card-body">
                                                @foreach($strands as $strand)

                                                    <div class="card mb-2">
                                                        <div class="card-header">
                                                            <h4>
                                                                {{ $strand['name'] }}
                                                                @if($strand['slot_type']=='limitted')
                                                                (<span class="badge bg-dark rounded-pill text-white">{{ $strand['available_slots'] }}</span> <small>Slots Available</small>)
                                                                @else
                                                                <small>(open)</small>
                                                                @endif
                                                                @if(Auth::user()->role=='student')
                                                                <form class="float-right" style="display: inline-block;" action="{{ route('applications_store',[ 'strand_id' => $strand['id'] ]) }}" method="POST">
                                                                    @csrf
                                                                    <a href="#" class="badge bg-success text-white" onclick="event.preventDefault(); this.closest('form').submit();">
                                                                        Submit Application
                                                                    </a>
                                                                </form>
                                                                @endif
                                                            </h4>
                                                        </div>
                                                    </div>

                                                @endforeach
                                            </div>
                                        </div>

                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
