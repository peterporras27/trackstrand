<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Register School') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    @if (session('success'))

                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Success!</h4>
                        <p>{{ session('success') }}</p>
                    </div>

                    @endif
                  
                    <form action="{{ route('schools_store') }}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="mb-3">
                                    <label for="logo" class="form-label">School Logo:</label>
                                    <input type="file" name="logo" class="form-control" id="logo">
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="mb-3">
                                    <label for="name" class="form-label">School Name:</label>
                                    <input type="text" name="name" class="form-control" id="name" placeholder="School Name">
                                </div>
                                <div class="mb-3">
                                    <label for="description" class="form-label">Description:</label>
                                    <textarea name="description" class="form-control" id="description" placeholder="Description"></textarea>
                                </div>
                            </div>
                            
                        </div>
                        <button class="btn btn-primary" style="background-color: #007bff;" type="submit">Save</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
