<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Schools') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    @if (session('success'))

                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Success!</h4>
                        <p>{{ session('success') }}</p>
                    </div>
                    @endif

                    <a href="{{ route('schools_create') }}" class="btn btn-info">Register School</a>

                    <br><br>

                    @if($schools->count())
                    <table class="table table-bordered table-inverse table-hover">
                        <thead>
                           <tr>
                               <th>Name</th>
                               <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                            @foreach($schools as $school)
                            <tr>
                                <td>
                                    @if($school->logo)
                                    <img width="50" style="display: inline-block;" src="{{ asset('storage/'.str_replace('public/','',$school->logo)) }}" class="img-fluid rounded-start img-thumbnail" alt="...">
                                    @endif
                                    {{ $school->name }}
                                </td>
                                <td class="align-middle">

                                    <div class="modal fade" id="modal-{{$school->id}}" tabindex="-1" aria-labelledby="modal-{{$school->id}}Label" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <form method="POST" action="{{ route('schools_destroy',$school->id) }}">
                                                    @csrf
                                                    @method('DELETE')
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="modal-{{$school->id}}Label">Delete School</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Are you sure you wish to premanently delete {{ $school->name }}?
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="#" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                                                    <a href="#" onclick="event.preventDefault();this.closest('form').submit();"
                                                        class="btn btn-danger">Delete</a>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <a href="#" class="float-right btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-{{$school->id}}">
                                        Delete
                                    </a>
                                    
                                    <a href="{{ route('schools_edit',$school->id) }}" class="float-right mr-2 btn btn-sm btn-info">Edit</a>
                                </td>
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
                    {{ $schools->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
