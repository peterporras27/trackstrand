<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',
        'phone',
        'birthday',
        'age',
        'gender',
        'civil_status',
        'address',
        'zip_code',
        'lrn',
        'birth_place',
        'parent_guardian',
        'occupation',
        'disabilities',
        'school_graduated_from',
        'last_school_attended',
        'last_school_address',
        'grade_year_level',
        'photo',
        'email',
        'password',
        'role',
        'school_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function application()
    {
        return $this->hasOne(Application::class,'id','user_id');
    }

    public function school()
    {
        return $this->hasOne(School::class,'id','school_id');
    }
}
