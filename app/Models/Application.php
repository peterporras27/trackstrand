<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory;

    protected $fillable = [
        'status'
    ];

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function strand(){
        return $this->hasOne(Strand::class,'id','strand_id');
    }

    public function school(){
        return $this->hasOne(School::class,'id','school_id');
    }
}
