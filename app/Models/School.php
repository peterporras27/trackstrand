<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'logo',
        'description'
    ];

    public function tracks(){
        $tracks = Track::all();
        $data = [];
        foreach ($tracks as $track) {
            $data[$track->name] = Strand::where([
                ['school_id', '=', $this->id],
                ['track_id', '=', $track->id]
            ])->get()->toArray();
        }

        return $data;
    }
}
