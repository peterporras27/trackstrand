<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Strand extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'available_slots',
        'slot_type',
        'track_id',
        'school_id'
    ];

    public function track(){
        return $this->hasOne(Track::class,'id','track_id');
    }

    public function school(){
        return $this->hasOne(School::class,'id','school_id');
    }
}
