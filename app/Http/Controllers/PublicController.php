<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\School;

class PublicController extends Controller
{
    public function index()
    {   
        $schools = School::paginate(10);
        return view('homepage',compact('schools'));
    }
}
