<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\School;
use App\Models\Strand;
use App\Models\Track;
use Auth;

class StrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        switch (Auth::user()->role) {
            case 'faculty':
                $strands = Strand::where('school_id','=',Auth::user()->school_id)->latest()->paginate(10);
                break;
            default:
                $strands = Strand::latest()->paginate(10);
                break;
        }

        
        return view('strands.index', compact('strands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tracks = Track::latest()->get();
        $schools = School::latest()->get();
        return view('strands.create',compact('tracks','schools'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        request()->validate([
            'name' => 'required|string',
            'track_id' => 'required|integer',
        ]);

        $strand = new Strand;
        $strand->fill($request->all());
        $strand->save();

        return redirect('/strands')->with('success','Data successfully saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $strand = Strand::find($id);
        $tracks = Track::all();
        $schools = School::all();
        return view('strands.edit', compact('strand','tracks','schools'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name' => 'required|string',
            'track_id' => 'required|integer',
            'available_slots' => 'required|integer'
        ]);

        $strand = Strand::find($id);
        $strand->fill($request->all());
        $strand->save();

        return redirect('/strands/edit/'.$id)->with('success','Data successfully saved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $strand = Strand::find($id);
        $strand->delete();

        return redirect('/strands')->with('success','Data successfully removed!');
    }
}
