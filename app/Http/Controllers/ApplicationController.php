<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Models\Application;
use App\Models\Strand;
use Auth;

class ApplicationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        switch (Auth::user()->role) {
            case 'student':
                $applications = Application::where([
                    ['user_id','=',Auth::user()->id],
                    ['year','=',date('Y')]
                ])->latest()->paginate(20);
                break;
            case 'faculty':
                $applications = Application::where([
                    ['school_id','=',Auth::user()->school_id],
                    ['year','=',date('Y')]
                ])->latest()->paginate(20);
                break;
            default:
                $applications = Application::where('year','=',date('Y'))->latest()->paginate(20);
                break;
        }

        return view('applications.index',compact('applications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $strand = Strand::find($request->input('strand_id'));

        // check if user has existing application
        $applied = Application::where([
            ['user_id','=',Auth::user()->id],
            ['year','=',date('Y')],
            ['status','!=','cancelled']
        ])->first();

        if ($applied) {
            return redirect('/dashboard')->with('danger','You have an existing '.$applied->status.' submission at the moment. Please wait for further updates.');
        }

        if (!$strand) {
            return redirect('/dashboard')->with('danger','Please try again.');
        }

        $app = new Application;
        $app->user_id = Auth::user()->id;
        $app->strand_id = $strand->id;
        $app->track_id = $strand->track_id;
        $app->school_id = $strand->school_id;
        $app->save();

        return redirect('/dashboard')->with('success','Application was successfully sent! please wait for updates with regards to your application.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $app = Application::find($id);
        $text = '';

        $hasPending = Application::where([
            ['id','!=',$id],
            ['user_id','=',$app->user_id],
            ['year','=',date('Y')],
            ['status','=','pending']
        ])->first();

        if ($hasPending) {
            return redirect('/applications')->with('info',$app->user->first_name.' '.$app->user->last_name.' already have an existing '.$hasPending->status.' submission at the moment.');
        }

        $hasApproved = Application::where([
            ['id','!=',$id],
            ['user_id','=',$app->user_id],
            ['year','=',date('Y')],
            ['status','=','approved']
        ])->first();

        if ($hasApproved) {
            return redirect('/applications')->with('info',$app->user->first_name.' '.$app->user->last_name.' already have an existing '.$hasApproved->status.' submission at the moment.');
        }

        if ($request->input('status')=='approved') {
            if ($app->status != 'approved' ) {
                $app->strand->available_slots = $app->strand->available_slots-1;
            }

            $text = '<p>Congratulations! your application has been approved.</p>';
        }
                
        if ($request->input('status')=='cancelled') {
            if ($app->status == 'approved') {
                $app->strand->available_slots = $app->strand->available_slots+1;
            }

            $text = '<p>Application has been rejected.</p><p>Kindly submit another application for your track and strand.</p>';
        }

        if (!empty($text)) 
        {
            $email = $app->user->email;
            $data = array(
                'name' => $app->user->first_name.' '.$app->user->last_name,
                'data' => $text
            );

            if (Auth::user()->role != 'student') {
                Mail::send('mail', $data, function($message) use ($email) {
                    $message->to($email, "Track and Strand Portal")->subject("Application Rejected");
                    $message->from('info@trackstrand.ml','Track and Strand Portal');
                });
            }
        }

        $app->strand->save();
        $app->status = $request->input('status');
        $app->save();

        return redirect('/applications')->with('success','Application status successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $app = Application::find($id);

        if ($app) { 

            $strand = Strand::find($app->strand_id);
            $strand->available_slots = $strand->available_slots-1;
            $strand->save();
            
            $email = $app->user->email;

            $data = array(
                'name' => $app->user->first_name.' '.$app->user->last_name,
                'data' => '<p>Application has been rejected.</p><p>Kindly submit another application for your track and strand.</p>'
            );

            Mail::send('mail', $data, function($message) use ($email) {
                $message->to($email, "Track and Strand Portal")->subject("Application Rejected");
                $message->from('info@trackstrand.ml','Track and Strand Portal');
            });

            $app->delete(); 
        }

        return redirect('/applications')->with('info','Application successfully rejected.');
    }
}
