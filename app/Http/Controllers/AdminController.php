<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\School;
use App\Models\User;
use Auth;

class AdminController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        switch (Auth::user()->role) {
            case 'student':
                $error = false;
                if(!Auth::user()->first_name) {$error = true;}
                if(!Auth::user()->last_name) {$error = true;}
                if(!Auth::user()->middle_name) {$error = true;}
                if(!Auth::user()->phone) {$error = true;}
                if(!Auth::user()->birthday) {$error = true;}
                if(!Auth::user()->age) {$error = true;}
                if(!Auth::user()->gender) {$error = true;}
                if(!Auth::user()->civil_status) {$error = true;}
                if(!Auth::user()->address) {$error = true;}
                if(!Auth::user()->zip_code) {$error = true;}
                if(!Auth::user()->lrn) {$error = true;}
                if(!Auth::user()->birth_place) {$error = true;}
                if(!Auth::user()->parent_guardian) {$error = true;}
                if(!Auth::user()->occupation) {$error = true;}
                if(!Auth::user()->disabilities) {$error = true;}
                if(!Auth::user()->school_graduated_from) {$error = true;}
                if(!Auth::user()->last_school_attended) {$error = true;}
                if(!Auth::user()->last_school_address) {$error = true;}
                if(!Auth::user()->grade_year_level) {$error = true;}
                if(!Auth::user()->photo) {$error = true;}

                if ($error) {
                    return redirect('/settings')->with('warning','Please fill up all your personal information.');
                }

                $schools = School::paginate(10);

                break;
            
            case 'faculty':
                
                $schools = School::where('id','=',Auth::user()->school_id)->paginate(10);
                break;
            default:

                $schools = School::paginate(10);
                break;
        }

        return view('dashboard',compact('schools'));
    }

    public function settings()
    {
        return view('users.settings');
    }

    public function save_settings(Request $request)
    {   
        request()->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'middle_name' => 'required|string',
            'phone' => 'required|string',
            'birthday' => 'required|date',
            'age' => 'required|integer',
            'gender' => 'required|string',
            'civil_status' => 'required|string',
            'address' => 'required|string',
            'zip_code' => 'required|integer',
            'lrn' => 'required|string',
            'birth_place' => 'required|string',
            'parent_guardian' => 'required|string',
            'occupation' => 'required|string',
            'disabilities' => 'required|string',
            'school_graduated_from' => 'required|string',
            'last_school_attended' => 'required|string',
            'last_school_address' => 'required|string',
            'grade_year_level' => 'required|string'
        ]);

        $datas = $request->all();
        $user = Auth::user();

        if ( $request->hasFile('photo') ) {

            $extension = $request->photo->extension();
            $allowed = ['png','jpg','jpeg','gif'];

            if ( !in_array($extension, $allowed) ) {
                return redirect('/settings')->with('error','File type not allowed, Please upload a valid image file.');
            }

            $image = $request->photo;  // your base64 encoded
            $imageName = Str::slug($datas['first_name'].' '.$datas['last_name']).'.'.'png';
            // Storage::disk('public')->put($imageName, $image);

            $datas['photo'] = $request->photo->store('public');

            if ($user->photo) {
                Storage::delete($user->photo);
            }

        } else {

            unset($datas['photo']);
        }

        $user->fill($datas);
        $user->save();

        return redirect('/settings')->with('success','Data successfully saved!');
    }

    public function user_index()
    {
        $users = User::where('role','=','faculty')->paginate(10);

        return view('users.index',compact('users'));
    }

    public function user_store(Request $request)
    {
        request()->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'middle_name' => 'required|string',
            'phone' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|confirmed',
            'school_id' => 'required|integer'
        ]);

        $user = new User;
        $user->fill($request->all());
        $user->password = Hash::make($request->password);
        $user->role = 'faculty';
        $user->save();

        return redirect('/users')->with('success','User successfully created!');
    }

    public function user_update(Request $request, $id)
    {
        $user = User::find($id);
        $data = $request->all();
        $validate = [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'middle_name' => 'required|string',
            'phone' => 'required',
            'school_id' => 'required|integer'
        ];

        if ($request->input('email') != $user->email) {
            $validate['email'] = 'required|string|email|max:255|unique:users';
        } else {
            unset($data['email']);
        }

        if (!empty($request->input('password')) ) {
            $validate['password'] = 'required|confirmed';
            $user->password = Hash::make($request->password);
        } else {
            unset($data['password']);
        }

        request()->validate($validate);
        $user->fill($data);
        $user->save();

        return redirect('/user/edit/'.$id)->with('success','User successfully updated!');
    }

    public function user_info(Request $request)
    {
        $user = User::find($request->input('id'));
        return view('users.info',compact('user'));
    }

    public function user_create(Request $request)
    {
        $schools = School::all();
        return view('users.create',compact('schools'));
    }

    public function user_edit($id)
    {
        $user = User::find($id);
        $schools = School::all();
        return view('users.edit',compact('user','schools'));
    }

    public function user_destroy($id)
    {
        $user = User::find($id);

        if (!$user) {
            return redirect('/users')->with('error','User no longer exist');
        }

        $user->delete();

        return redirect('/users')->with('success','User successfully removed.');
    }
}
