<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Application;
use App\Models\School;
use App\Models\Strand;

class SchoolsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schools = School::paginate(10);
        return view('schools.index', compact('schools'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('schools.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        request()->validate([
            'name' => 'required|string'
        ]);

        $datas = $request->all();

        if ( $request->hasFile('logo') ) {

            $extension = $request->logo->extension();
            $allowed = ['png','jpg','jpeg','gif'];

            if ( !in_array($extension, $allowed) ) {
                return redirect('/school/create')->with('error','File type not allowed, Please upload a valid image file.');
            }

            $image = $request->logo;  // your base64 encoded
            $datas['logo'] = $request->logo->store('public');

        } else {

            unset($datas['logo']);
        }

        $school = new School;
        $school->fill($datas);
        $school->save();

        return redirect('/schools')->with('success','Data successfully saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $school = School::find($id);

        return view('schools.edit', compact('school'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name' => 'required|string'
        ]);

        $school = School::find($id);
        $datas = $request->all();

        if ( $request->hasFile('logo') ) {

            $extension = $request->logo->extension();
            $allowed = ['png','jpg','jpeg','gif'];

            if ( !in_array($extension, $allowed) ) {
                return redirect('/schools/edit'.$id)->with('error','File type not allowed, Please upload a valid image file.');
            }

            $image = $request->logo;  // your base64 encoded
            $datas['logo'] = $request->logo->store('public');

            if ($school->logo) {
                Storage::delete($school->logo);
            }

        } else {

            unset($datas['logo']);
        }

        $school->fill($datas);
        $school->save();

        return redirect('/schools/edit/'.$id)->with('success','Data successfully saved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $school = School::find($id);
        $school->delete();

        $apps = Application::where('school_id','=',$id)->delete();
        $strands = Strand::where('school_id','=',$id)->delete();

        return redirect('/schools')->with('success','Data successfully removed!');
    }
}
